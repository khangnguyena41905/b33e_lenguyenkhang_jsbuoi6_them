function handle_5() {
  const n = document.getElementById("txt-n-nt").value * 1;
  var luuSoNt = "";
  if (n >= 2) {
    for (var i = 2; i <= n; i++) {
      var checkDk = true; // biến checkDk dùng để kiểm tra điều kiện, nếu số i là số nt thì biến checkDk sẽ là True ngược lại là False
      for (var j = 2; j <= Math.sqrt(i); j++) {
        if (i % j == 0) {
          j = Math.sqrt(i) + 1; // set điều kiện để thoát vòng lặp ngay
          checkDk = false;
        }
      }
      if (checkDk == true) {
        luuSoNt += `${i} `;
      }
    }
    document.getElementById(
      "result5"
    ).innerHTML = `<h1> Các số nguyên tố là: ${luuSoNt} </h1>`;
  } else {
    document.getElementById("result5").innerHTML = `<h1> Nhập lại số n </h1>`;
  }
}
